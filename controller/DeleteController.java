package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dao.StudentRepo;

@Controller
public class DeleteController 
{

	@Autowired
	StudentRepo repo;
	
	@RequestMapping("/delete")
	public String delete(@RequestParam int rno)
	{
		repo.deleteById(rno);
		return "home.jsp";
	}
}
