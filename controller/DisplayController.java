package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.bean.Student;
import com.example.demo.dao.StudentRepo;

@Controller
public class DisplayController 
{
	@Autowired
	StudentRepo repo;
	
	
	@RequestMapping("/display")
	public ModelAndView display()
	{
		ModelAndView mv = new ModelAndView();
		
		//Student student = repo.findById(rno).orElse(new Student());
		Iterable<Student> student = repo.findAll();
		
		mv.addObject("student", student);
		mv.setViewName("display.jsp");
		return mv;
	}
	
}
