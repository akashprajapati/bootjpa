package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.bean.Student;
import com.example.demo.dao.StudentRepo;

@Controller
public class InsertController 
{
	@Autowired
	StudentRepo repo;
	
	@RequestMapping("/add")
	public String insert(Student st)
	{
		repo.save(st);
		return "home.jsp";
	}
}
